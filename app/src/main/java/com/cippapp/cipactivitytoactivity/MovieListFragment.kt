package com.cippapp.cipactivitytoactivity

import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.cippapp.cipactivitytoactivity.adapter.MovieAdapter
import com.cippapp.cipactivitytoactivity.databinding.FragmentMovieListBinding
import com.cippapp.cipactivitytoactivity.view_model.MovieItemsViewModel

// TODO: Rename parameter arguments, choose names that match
// the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
private const val ARG_PARAM1 = "param1"
private const val ARG_PARAM2 = "param2"

/**
 * A simple [Fragment] subclass.
 * Use the [MovieListFragment.newInstance] factory method to
 * create an instance of this fragment.
 */
class MovieListFragment : Fragment() {
    // TODO: Rename and change types of parameters
    private var param1: String? = null
    private var param2: String? = null

    private lateinit var binding: FragmentMovieListBinding
    var recyclerView: RecyclerView? = null
    var manager: GridLayoutManager? = null
    var adapter: MovieAdapter? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {
            param1 = it.getString(ARG_PARAM1)
            param2 = it.getString(ARG_PARAM2)
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = FragmentMovieListBinding.inflate(inflater,container,false)
        val view = binding.root

        var data = ArrayList<MovieItemsViewModel>()
        data.add(MovieItemsViewModel("https://akns-images.eonline.com/eol_images/Entire_Site/2018112/rs_634x939-181202195654-634.captain-marvel.12418.jpg","Captain Marvel"))
        data.add(MovieItemsViewModel("https://images.thedirect.com/media/photos/posd1_1.jpg","Black Adam"))
        data.add(MovieItemsViewModel("https://assets-global.website-files.com/6009ec8cda7f305645c9d91b/6408f6e7b5811271dc883aa8_batman-min.png","Batman"))
        data.add(MovieItemsViewModel("https://d1csarkz8obe9u.cloudfront.net/posterpreviews/movie-poster-template-design-21a1c803fe4ff4b858de24f5c91ec57f_screen.jpg?ts=1636996180","After"))
        data.add(MovieItemsViewModel("https://marketplace.canva.com/EAFMqwkPfOY/2/0/1131w/canva-black-minimalist-horror-movie-poster-3bttgZhMDnA.jpg","The Dark"))
        data.add(MovieItemsViewModel("https://m.media-amazon.com/images/I/81F5PF9oHhL._AC_UF894,1000_QL80_.jpg","John Wick"))
        Log.d("cip","ini data: $data")

        recyclerView = view.findViewById<View>(R.id.rv_movieList) as RecyclerView
        manager = GridLayoutManager(activity, 2)
        recyclerView!!.layoutManager= manager
        adapter = context?.let { MovieAdapter(it,data) }
        recyclerView!!.adapter = adapter
        // Inflate the layout for this fragment
        return view
    }

    companion object {
        /**
         * Use this factory method to create a new instance of
         * this fragment using the provided parameters.
         *
         * @param param1 Parameter 1.
         * @param param2 Parameter 2.
         * @return A new instance of fragment OnboardingOneFragment.
         */
        // TODO: Rename and change types and number of parameters
        @JvmStatic
        fun newInstance(param1: String, param2: String) =
            MovieListFragment().apply {
                arguments = Bundle().apply {
                    putString(ARG_PARAM1, param1)
                    putString(ARG_PARAM2, param2)
                }
            }
    }
}