package com.cippapp.cipactivitytoactivity

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View

class OnBoardingOneActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_on_boarding_one)
    }

    fun intentToOnBoardingTwo(view: View) {
        startActivity(Intent(this,OnBoardingTwoActivity::class.java))
    }
}